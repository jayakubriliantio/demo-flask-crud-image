from flask import redirect, url_for
from flask_login import current_user
import functools

class RoleChecker:

  def check_permission(self, role):
    def decorator_check_permission(func):
      @functools.wraps(func)
      def wrapper_check_permission(*args, **kwargs):
        role_name = current_user.role
        if role != role_name:
          return redirect(url_for('error.unauthorized'))

        return func(*args, **kwargs)
      
      return wrapper_check_permission
    
    return decorator_check_permission