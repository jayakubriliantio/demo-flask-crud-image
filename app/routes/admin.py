from flask import Blueprint, request, redirect, render_template, url_for
from app.controllers.book import BookController

admin = Blueprint('admin', __name__, template_folder='templates')
book_controller = BookController()


@admin.route('/', methods=['GET'])
def index():
  books = book_controller.fetch_all()
  return render_template('admin/index.html', data={
    'books': books
  })


@admin.route('/book/create', methods=['POST'])
def book_create():
  book_controller.create(
    data=request.form,
    photo=request.files['image']
  )
  return redirect(url_for('admin.index'))


@admin.route('/book/update', methods=['POST'])
def book_update():
  book_controller.update(
    data=request.form,
    photo=request.files['image']
  )
  return redirect(url_for('admin.index'))


@admin.route('/book/delete/<int:id>', methods=['GET'])
def book_delete(id):
  book_controller.delete(id=id)
  return redirect(url_for('admin.index'))

