from app import db

class Book(db.Model):
  id = db.Column(db.Integer, primary_key=True, autoincrement=True)
  title = db.Column(db.String(255), unique=False, nullable=False)
  writer = db.Column(db.String(255), unique=False, nullable=False)
  publisher = db.Column(db.Text, unique=False, nullable=False)
  image_filename = db.Column(db.String(255), unique=False, nullable=False)

  def create(self, title, writer, publisher, image_filename):
    book = Book(
      title=title,
      writer=writer,
      publisher=publisher,
      image_filename=image_filename
    )

    db.session.add(book)
    db.session.commit()


  def update(self, id, title, writer, publisher, image_filename=None):
    book = Book.query.filter_by(id=id).first()
    book.title = title
    book.writer = writer
    book.publisher = publisher

    if image_filename is not None:
      book.image_filename = image_filename

    db.session.commit()

  
  def delete(self, id):
    book = Book.query.filter_by(id=id).first()
    
    db.session.delete(book)
    db.session.commit()

    