from app import db
from flask_login import UserMixin


class User(UserMixin, db.Model):
  id = db.Column(db.Integer, primary_key=True, autoincrement=True)
  username = db.Column(db.String(255), nullable=False, unique=True)
  name = db.Column(db.String(255), nullable=False, unique=True)
  password = db.Column(db.String(255), nullable=False)
  role = db.Column(db.String(255), nullable=False)