from flask import render_template, redirect, url_for
from app.models.book import Book
from app.utils.image import delete_image, save_image

class BookController:

  def __init__(self):
    self.book = Book()

  def fetch_all(self):
    return self.book.query.all()

  def fetch_by_id(self, id):
    return self.book.query.filter_by(id=id).first()

  def create(self, data, photo):

    image = save_image(photo)

    self.book.create(
      title=data['title'],
      writer=data['writer'],
      publisher=data['publisher'],
      image_filename=image
    )

  def update(self, data, photo):
    book = self.book.query.filter_by(id=data['id']).first()
    image = save_image(photo) if photo else None
    delete_image(book.image_filename) if image is not None else None

    self.book.update(
      id=data['id'],
      title=data['title'],
      writer=data['writer'],
      publisher=data['publisher'],
      image_filename=image
    )

  def delete(self, id):
    book = self.book.query.filter_by(id=id).first()
    delete_image(book.image_filename)
    self.book.delete(id=id)

